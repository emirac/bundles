﻿using Bundles.Entities;
using Bundles.Models;
using Bundles.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bundles.Controllers
{
    [ApiController]
    public class ProductsController
    {
        private readonly BundlesContext _context;
        private BundlesService _bundlesService { get; set; }

        public ProductsController(BundlesContext context)
        {
            this._context = context;
            this._bundlesService = new BundlesService();
        }

        [HttpPost]
        [Route("api/Bundles/{bundleId}/[controller]")]
        public async Task<ActionResult<ProductDto>> Create(int bundleId, ProductDto productDto)
        {
            var bundle = await this._context.Bundles
                .Include(x => x.Products)
                .FirstOrDefaultAsync(b => b.ID == bundleId);

            if (bundle == null)
            {
                return new NotFoundResult();
            }

            var bundleDto = new BundleDto(bundle);
            bundleDto.Products.AddRange(bundle.Products.Select(p => new ProductDto(p)));
            
            var allowed = this._bundlesService.CanAddProduct(bundleDto, productDto, out List<string> errors);
            if (!allowed)
            {
                return new ConflictObjectResult(new { errors });
            }

            var product = new Product();
            product.BundleID = bundleId;
            product.Name = productDto.Name;

            this._context.Products.Add(product);
            await this._context.SaveChangesAsync();
            return new OkResult();
        }

        [HttpGet]
        [Route("api/Bundles/{bundleId}/[controller]")]
        public async Task<ActionResult<List<ProductDto>>> List(int bundleId)
        {
            var products = await this._context.Products
                .Where(p => p.BundleID == bundleId)
                .Select(pr => new ProductDto(pr))
                .ToListAsync();

            return products;
        }

        [HttpGet]
        [Route("api/Bundles/{bundleId}/[controller]/{id}")]
        public async Task<ActionResult<ProductDto>> Get(int bundleId, int id)
        {
            var product = await this._context.Products
                .FirstOrDefaultAsync(b => b.ID == id);

            if (product == null)
            {
                return new NotFoundResult();
            }

            var productDto = new ProductDto(product);
            return productDto;
        }

        [HttpPut]
        [Route("api/Bundles/{bundleId}/[controller]/{id}")]
        public async Task<ActionResult> Update(int bundleId, int productId, ProductDto productDto)
        {
            var product = await this._context.Products
                .FirstOrDefaultAsync(p => p.ID == productDto.Id);
            var bundle = await this._context.Bundles
                .Include(x => x.Products.Where(p => p.ID != productId))
                .FirstOrDefaultAsync(b => b.ID == bundleId);

            if (product == null || bundle == null)
            {
                return new NotFoundResult();
            }

            var bundleDto = new BundleDto(bundle);
            bundleDto.Products.AddRange(bundle.Products.Select(p => new ProductDto(p)));

            var allowed = this._bundlesService.CanAddProduct(bundleDto, productDto, out List<string> errors);
            if (!allowed)
            {
                return new ConflictObjectResult(new { errors });
            }

            product.Name = productDto.Name;
            this._context.Update(product);
            await this._context.SaveChangesAsync();

            return new OkResult();
        }

        [HttpDelete]
        [Route("api/Bundles/{bundleId}/[controller]/{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            var product = await this._context.Products.FirstOrDefaultAsync(p => p.ID == id);

            if (product == null)
            {
                return new NotFoundResult();
            }

            this._context.Products.Remove(product);
            await this._context.SaveChangesAsync();
            return product.ID;
        }
    }
}
