﻿using Bundles.Entities;
using Bundles.Models;
using Bundles.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bundles.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BundlesController : ControllerBase
    {
        private readonly BundlesContext _context;
        private BundlesService _bundlesService { get; set; }

        public BundlesController(BundlesContext context)
        {
            this._context = context;
            this._bundlesService = new BundlesService();
        }

        [HttpPost]
        public async Task<ActionResult<BundleDto>> Create(BundleDto bundle)
        {
            var defaultBundle = this._bundlesService.GetDefaultBundle(bundle);
            var b = new Bundle()
            {
                Age = bundle.Age,
                BundleName = defaultBundle.Name,
                Income = bundle.Income,
                Student = bundle.Student
            };

            foreach (var p in defaultBundle.Products)
            {
                b.Products.Add(new Product() { Name = p });
            }

            this._context.Bundles.Add(b);
            await this._context.SaveChangesAsync();

            return new CreatedAtActionResult(nameof(Get), "Bundles", new { id = b.ID }, b);
        }

        [HttpGet]
        public async Task<ActionResult<List<BundleDto>>> List()
        {
            var bundles = await this._context.Bundles
                .Select(b => new BundleDto(b))
                .ToListAsync();
            return bundles;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BundleDto>> Get(int id)
        {
            var bundle = await this._context.Bundles
                .Include(b => b.Products)
                .FirstOrDefaultAsync(b => b.ID == id);

            if (bundle == null)
            {
                return new NotFoundResult();
            }

            var bundleDto = new BundleDto(bundle);
            bundleDto.Products.AddRange(bundle.Products.Select(x => new ProductDto(x)));
            return bundleDto;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            var bundle = await this._context.Bundles.FirstOrDefaultAsync(b => b.ID == id);

            if (bundle == null)
            {
                return new NotFoundResult();
            }

            this._context.Bundles.Remove(bundle);
            await this._context.SaveChangesAsync();
            return bundle.ID;
        }
    }
}
