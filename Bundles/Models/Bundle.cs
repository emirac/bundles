﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bundles.Models
{
    public class Bundle
    {
        public int ID { get; set; }
        public string BundleName { get; set; }
        public int Age { get; set; }
        public bool Student { get; set; }
        public long Income { get; set; }

        public ICollection<Product> Products { get; set; }

        public Bundle()
        {
            Products = new Collection<Product>();
        }
    }
}
