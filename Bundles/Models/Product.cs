﻿namespace Bundles.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int BundleID { get; set; }

        public virtual Bundle Bundle { get; set; }
    }
}
