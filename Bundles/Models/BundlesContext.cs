﻿using Microsoft.EntityFrameworkCore;

namespace Bundles.Models
{
    public class BundlesContext : DbContext
    {
        public BundlesContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Bundle> Bundles { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
