﻿using Bundles.Entities;
using Bundles.Entities.DefaultBundles;
using Bundles.Entities.DefaultProducts;
using Bundles.Entities.Helpers;
using Bundles.Services.Interfaces;
using System.Collections.Generic;

namespace Bundles.Services
{
    public class BundlesService : IBundleService
    {
        DefaultBundles DefaultBundles { get; set; }
        DefaultProducts DefaultProducts { get; set; }
        public BundlesService()
        {
            this.DefaultBundles = new DefaultBundles();
            this.DefaultProducts = new DefaultProducts();
        }

        public DefaultBundle GetDefaultBundle(BundleDto bundle)
        {
            var defaults = this.DefaultBundles.GetOrderedBundles();
            foreach (var d in defaults)
            {
                if (d.AreConditionsMet(bundle))
                {
                    return d;
                }
            }
            return new DefaultBundle();
        }

        public bool CanAddProduct(BundleDto bundle, ProductDto product, out List<string> errorMessages)
        {
            errorMessages = new List<string>();
            var defaultProd = this.DefaultProducts.GetProduct(product.Name);
            if (defaultProd == null)
            {
                errorMessages.Add(ErrorsHelper.NonExistingProduct);
            }

            var account = bundle.Products.Find(p => ProductsHelper.Accounts.Contains(p.Name));
            if (ProductsHelper.Accounts.Contains(product.Name) && account != null)
            {
                errorMessages.Add(ErrorsHelper.OneAccountAllowed);
            }

            defaultProd.AreConditionsMet(bundle, ref errorMessages);
            return errorMessages.Count == 0;
        }
    }
}
