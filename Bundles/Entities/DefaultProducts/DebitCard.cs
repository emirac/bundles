﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class DebitCard : DefaultProduct
    {
        public DebitCard()
        {
            this.Name = ProductsHelper.DebitCard;
            this.AddCondition(new HasAccountCondition());
        }
    }
}
