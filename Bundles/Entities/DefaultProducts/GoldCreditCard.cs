﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class GoldCreditCard : DefaultProduct
    {
        public GoldCreditCard()
        {
            this.Name = ProductsHelper.GoldCreditCard;
            this.AddCondition(new HighIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
        }
    }
}
