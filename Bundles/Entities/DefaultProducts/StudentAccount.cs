﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class StudentAccount : DefaultProduct
    {
        public StudentAccount()
        {
            this.Name = ProductsHelper.StudentAccount;
            this.AddCondition(new IsStudentCondition());
            this.AddCondition(new MinimalAgeCondition());
        }
    }
}
