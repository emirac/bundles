﻿using Bundles.Entities.Conditions;

namespace Bundles.Entities.DefaultProducts
{
    public class DefaultProduct : DefaultItem
    {
        public string Name { get; set; }
    }
}
