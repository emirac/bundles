﻿using System.Collections.Generic;

namespace Bundles.Entities.DefaultProducts
{
    public class DefaultProducts
    {
        private List<DefaultProduct> Products { get; } = new List<DefaultProduct>();
        public DefaultProducts()
        {
            this.Products.Add(new CreditCard());
            this.Products.Add(new CurrentAccount());
            this.Products.Add(new CurrentAccountPlus());
            this.Products.Add(new DebitCard());
            this.Products.Add(new GoldCreditCard());
            this.Products.Add(new JuniorSaverAccount());
            this.Products.Add(new StudentAccount());
        }

        public DefaultProduct GetProduct(string name)
        {
            return this.Products.Find(p => p.Name == name);
        }
    }
}
