﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class CreditCard : DefaultProduct
    {
        public CreditCard()
        {
            this.Name = ProductsHelper.CreditCard;
            this.AddCondition(new MediumIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
        }
    }
}
