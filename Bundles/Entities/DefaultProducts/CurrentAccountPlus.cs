﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class CurrentAccountPlus : DefaultProduct
    {
        public CurrentAccountPlus()
        {
            this.Name = ProductsHelper.CurrentAccountPlus;
            this.AddCondition(new HighIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
        }
    }
}
