﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class JuniorSaverAccount : DefaultProduct
    {
        public JuniorSaverAccount()
        {
            this.Name = ProductsHelper.JuniorSaverAccount;
            this.AddCondition(new NonMediumAgeCondition());
        }
    }
}
