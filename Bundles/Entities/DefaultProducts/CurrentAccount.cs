﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultProducts
{
    public class CurrentAccount : DefaultProduct
    {
        public CurrentAccount()
        {
            this.Name = ProductsHelper.CurrentAccount;
            this.AddCondition(new MinimalIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
        }
    }
}
