﻿using Bundles.Models;

namespace Bundles.Entities
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BundleId { get; set; }

        public BundleDto Bundle { get; set; }

        public ProductDto(Product product)
        {
            this.Id = product.ID;
            this.Name = product.Name;
            this.BundleId = product.BundleID;
        }
    }
}
