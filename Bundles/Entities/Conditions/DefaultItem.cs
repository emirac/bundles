﻿using System.Collections.Generic;

namespace Bundles.Entities.Conditions
{
    public class DefaultItem
    {
        private Dictionary<string, Condition> Conditions { get; } = new Dictionary<string, Condition>();

        public void AddCondition(Condition condition)
        {
            var o = this.Conditions.TryGetValue(nameof(condition), out Condition old);
            if (old == null)
            { 
                this.Conditions.Add(condition.GetType().Name, condition);
            }
        }

        public bool AreConditionsMet(BundleDto answer)
        {
            foreach (var condition in Conditions)
            {
                if (!condition.Value.IsConditionMet(answer))
                {
                    return false;
                }
            }
            return true;
        }

        public void AreConditionsMet(BundleDto answer, ref List<string> errorMessages)
        {
            foreach (var condition in Conditions)
            {
                if (!condition.Value.IsConditionMet(answer))
                {
                    errorMessages.Add(condition.Value.ErrorMessage);
                }
            }
        }
    }
}
