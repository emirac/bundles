﻿using Bundles.Entities.Helpers;

namespace Bundles.Entities.Conditions
{
    public class MediumAgeCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.MediumAge;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Age > AgeHelper.MediumAgeMin;
        }
    }
}
