﻿using Bundles.Entities.Helpers;

namespace Bundles.Entities.Conditions
{
    public class IsStudentCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.StudentRequired;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Student;
        }
    }
}
