﻿using Bundles.Entities.Helpers;

namespace Bundles.Entities.Conditions
{
    public class NonMinimalAgeCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.AdultAge;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Age > AgeHelper.MinimalAgeMax;
        }
    }
}
