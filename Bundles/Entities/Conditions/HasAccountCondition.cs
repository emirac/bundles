﻿using Bundles.Entities.Helpers;
using System.Linq;

namespace Bundles.Entities.Conditions
{
    public class HasAccountCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.AccountRequired;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Products.Any() && answer.Products.FirstOrDefault(a => ProductsHelper.Accounts.Contains(a.Name)) != null;
        }
    }
}
