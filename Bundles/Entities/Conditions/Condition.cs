﻿using Bundles.Services.Interfaces;

namespace Bundles.Entities.Conditions
{
    public abstract class Condition : ICondition
    {
        public abstract string ErrorMessage { get; }
        public abstract bool IsConditionMet(BundleDto answer);
    }
}
