﻿using Bundles.Entities.Helpers;

namespace Bundles.Entities.Conditions
{
    public class HighIncomeCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.NonSufficientIncome;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Income > IncomeHelper.HighIncomeMin;
        }
    }
}
