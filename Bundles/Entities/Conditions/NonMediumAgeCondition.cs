﻿using Bundles.Entities.Helpers;

namespace Bundles.Entities.Conditions
{
    public class NonMediumAgeCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.StudentAge;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Age < AgeHelper.MediumAgeMin;
        }
    }
}
