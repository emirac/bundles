﻿using Bundles.Entities.Helpers;

namespace Bundles.Entities.Conditions
{
    public class MinimalAgeCondition : Condition
    {
        public override string ErrorMessage => ErrorsHelper.MinorAge;

        public override bool IsConditionMet(BundleDto answer)
        {
            return answer.Age < AgeHelper.MinimalAgeMax;
        }
    }
}
