﻿using Bundles.Entities;
using Bundles.Entities.DefaultBundles;
using System.Collections.Generic;

namespace Bundles.Services.Interfaces
{
    public interface IBundleService
    {
        DefaultBundle GetDefaultBundle(BundleDto bundle);
        bool CanAddProduct(BundleDto bundle, ProductDto product, out List<string> errorMessages);
    }
}
