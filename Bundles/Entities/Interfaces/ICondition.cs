﻿using Bundles.Entities;

namespace Bundles.Services.Interfaces
{
    public interface ICondition
    {
        bool IsConditionMet(BundleDto item);
    }
}
