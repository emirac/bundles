﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultBundles
{
    public class Gold : DefaultBundle
    {
        public Gold()
        {
            this.Name = BundlesHelper.Gold;
            this.Rank = BundlesHelper.Ranks[BundlesHelper.Gold];
            this.AddCondition(new HighIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
            this.Products.Add(ProductsHelper.CurrentAccountPlus);
            this.Products.Add(ProductsHelper.DebitCard);
            this.Products.Add(ProductsHelper.GoldCreditCard);
        }
    }
}
