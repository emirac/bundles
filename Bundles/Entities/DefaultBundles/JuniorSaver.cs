﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultBundles
{
    public class JuniorSaver : DefaultBundle
    {
        public JuniorSaver()
        {
            this.Name = BundlesHelper.JuniorSaver;
            this.Rank = BundlesHelper.Ranks[BundlesHelper.JuniorSaver];
            this.AddCondition(new NonMediumAgeCondition());
            this.Products.Add(ProductsHelper.JuniorSaverAccount);
        }
    }
}
