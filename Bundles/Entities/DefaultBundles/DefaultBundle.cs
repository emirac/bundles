﻿using Bundles.Entities.Conditions;
using Bundles.Entities.DefaultProducts;
using System.Collections.Generic;

namespace Bundles.Entities.DefaultBundles
{
    public class DefaultBundle : DefaultItem
    {
        public string Name { get; set; }
        public int Rank { get; set; }
        public List<string> Products { get; } = new List<string>();
    }
}
