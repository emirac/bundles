﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultBundles
{
    public class Student : DefaultBundle
    {
        public Student()
        {
            this.Name = BundlesHelper.Student;
            this.Rank = BundlesHelper.Ranks[BundlesHelper.Student];
            this.AddCondition(new IsStudentCondition());
            this.AddCondition(new NonMinimalAgeCondition());
            this.Products.Add(ProductsHelper.StudentAccount);
            this.Products.Add(ProductsHelper.DebitCard);
            this.Products.Add(ProductsHelper.CreditCard);
        }
    }
}
