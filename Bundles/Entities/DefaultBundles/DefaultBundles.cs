﻿using System.Collections.Generic;
using System.Linq;

namespace Bundles.Entities.DefaultBundles
{
    public class DefaultBundles
    {
        private List<DefaultBundle> Bundles { get; } = new List<DefaultBundle>();
        public DefaultBundles()
        {
            this.Bundles.Add(new JuniorSaver());
            this.Bundles.Add(new Student());
            this.Bundles.Add(new Classic());
            this.Bundles.Add(new ClassicPlus());
            this.Bundles.Add(new Gold());
        }

        public List<DefaultBundle> GetOrderedBundles()
        {
            return this.Bundles.OrderByDescending(x => x.Rank).ToList();
        }
    }
}
