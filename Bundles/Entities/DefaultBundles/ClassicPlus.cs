﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultBundles
{
    public class ClassicPlus : DefaultBundle
    {
        public ClassicPlus()
        {
            this.Name = BundlesHelper.ClassicPlus;
            this.Rank = BundlesHelper.Ranks[BundlesHelper.ClassicPlus];
            this.AddCondition(new MediumIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
            this.Products.Add(ProductsHelper.CurrentAccount);
            this.Products.Add(ProductsHelper.DebitCard);
            this.Products.Add(ProductsHelper.CreditCard);
        }
    }
}
