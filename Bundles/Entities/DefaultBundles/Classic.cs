﻿using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;

namespace Bundles.Entities.DefaultBundles
{
    public class Classic : DefaultBundle
    {
        public Classic()
        {
            this.Name = BundlesHelper.Classic;
            this.Rank = BundlesHelper.Ranks[BundlesHelper.Classic];
            this.AddCondition(new MediumIncomeCondition());
            this.AddCondition(new NonMinimalAgeCondition());
            this.Products.Add(ProductsHelper.CurrentAccount);
            this.Products.Add(ProductsHelper.DebitCard);
            this.Products.Add(ProductsHelper.CreditCard);
        }
    }
}
