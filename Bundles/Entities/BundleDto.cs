﻿using Bundles.Models;
using System.Collections.Generic;

namespace Bundles.Entities
{
    public class BundleDto
    {
        public int Id { get; set; }
        public string BundleName { get; set; }
        public int Age { get; set; }
        public bool Student { get; set; }
        public long Income { get; set; }

        public List<ProductDto> Products { get; } = new List<ProductDto>();

        public BundleDto(Bundle bundle)
        {
            this.Id = bundle.ID;
            this.Income = bundle.Income;
            this.Student = bundle.Student;
            this.Age = bundle.Age;
            this.BundleName = bundle.BundleName;
        }
    }
}
