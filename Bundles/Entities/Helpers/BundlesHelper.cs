﻿using System.Collections.Generic;

namespace Bundles.Entities.Helpers
{
    public class BundlesHelper
    {
        public const string JuniorSaver = "JuniorSaver";
        public const string Student = "Student";
        public const string Classic = "Classic";
        public const string ClassicPlus = "ClassicPlus";
        public const string Gold = "Gold";

        public static Dictionary<string, int> Ranks => new Dictionary<string, int>()
        {
            { JuniorSaver, 0 },
            { Student, 0 },
            { Classic, 1 },
            { ClassicPlus, 2 },
            { Gold, 3 }
        };
    }

}
