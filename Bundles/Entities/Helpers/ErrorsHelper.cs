﻿namespace Bundles.Entities.Helpers
{
    public class ErrorsHelper
    {
        public const string AdultAge = "You have to be an adult";
        public const string NonSufficientIncome = "Income is not sugfficient";
        public const string MediumAge = "You have to be older than 181";
        public const string StudentAge = "You have to be younger than 18";
        public const string MinorAge = "You have to be younger than 17";
        public const string StudentRequired = "You have to be a student";
        public const string AccountRequired = "You have to have an account";
        public const string NonExistingProduct = "Selected product does not exist";
        public const string OneAccountAllowed = "You are allowed to have only one account";
    }
}
