﻿namespace Bundles.Entities.Helpers
{
    public class IncomeHelper
    {
        public const long MinimalIncomeMin = 0;
        public const long MinimalIncomeMax = 12000;

        public const long MediumIncomeMin = 12001;
        public const long MediumIncomeMax = 40000;

        public const long HighIncomeMin = 40001;
    }
}
