﻿namespace Bundles.Entities.Helpers
{
    public class AgeHelper
    {
        public const int MinimalAgeMin = 0;
        public const int MinimalAgeMax = 17;

        public const int MediumAgeMin = 18;
        public const int MediumAgeMax = 64;

        public const int HighAgeMin = 65;
    }
}
