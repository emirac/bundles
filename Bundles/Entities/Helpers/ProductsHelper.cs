﻿using System.Collections.Generic;

namespace Bundles.Entities.Helpers
{
    public class ProductsHelper
    {
        public const string CurrentAccount = "CurrentAccount";
        public const string CurrentAccountPlus = "CurrentAccountPlus";
        public const string JuniorSaverAccount = "JuniorSaverAccount";
        public const string StudentAccount = "StudentAccount";
        public const string PenssionerAccount = "PenssionerAccount";
        public const string DebitCard = "DebitCard";
        public const string CreditCard = "CreditCard";
        public const string GoldCreditCard = "GoldCreditCard";

        public static List<string> Accounts => new List<string>()
        {
            CurrentAccount,
            CurrentAccountPlus,
            StudentAccount,
            PenssionerAccount
        };
    }
}
