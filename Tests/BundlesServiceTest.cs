﻿using Bundles.Entities;
using Bundles.Entities.DefaultBundles;
using Bundles.Entities.Helpers;
using Bundles.Services;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class BundlesServiceTest
    {
        [Fact]
        public void GetDefaultBundleTest()
        {
            var service = new BundlesService();

            var bundle1 = new BundleDto();
            bundle1.Age = 25;
            bundle1.Income = 15000;

            Assert.True(service.GetDefaultBundle(bundle1).GetType().Name == typeof(ClassicPlus).Name);
        }

        [Fact]
        public void CanAddProductTest()
        {
            var service = new BundlesService();

            var bundle1 = new BundleDto();
            bundle1.Age = 25;
            bundle1.Income = 15000;

            var product = new ProductDto();
            product.Name = ProductsHelper.JuniorSaverAccount;

            Assert.False(service.CanAddProduct(bundle1, product, out List<string> errorMessages));
        }
    }
}
