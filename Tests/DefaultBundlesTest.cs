﻿using Bundles.Entities;
using Bundles.Entities.DefaultBundles;
using Xunit;

namespace Tests
{
    public class DefaultBundlesTest
    {
        [Fact]
        public void JuniorSaverBundleTest()
        {
            var bundle = new BundleDto();
            var juniorSaver = new JuniorSaver();

            bundle.Age = 16;
            Assert.True(juniorSaver.AreConditionsMet(bundle));

            bundle.Age = 26;
            Assert.False(juniorSaver.AreConditionsMet(bundle));
        }

        [Fact]
        public void StudentBundleTest()
        {
            var bundle = new BundleDto();
            var student = new Student();

            bundle.Age = 27;
            bundle.Student = true;
            Assert.True(student.AreConditionsMet(bundle));

            bundle.Age = 14;
            Assert.False(student.AreConditionsMet(bundle));
        }

        [Fact]
        public void ClassicBundleTest()
        {
            var bundle = new BundleDto();
            var classic = new Classic();

            bundle.Age = 27;
            bundle.Income = 55000;
            Assert.True(classic.AreConditionsMet(bundle));

            bundle.Income = 550;
            Assert.False(classic.AreConditionsMet(bundle));
        }

        [Fact]
        public void ClassicPlusBundleTest()
        {
            var bundle = new BundleDto();
            var classicPlus = new ClassicPlus();

            bundle.Age = 27;
            bundle.Income = 55000;
            Assert.True(classicPlus.AreConditionsMet(bundle));

            bundle.Age = 15;
            Assert.False(classicPlus.AreConditionsMet(bundle));
        }

        [Fact]
        public void GoldBundleTest()
        {
            var bundle = new BundleDto();
            var gold = new Gold();

            bundle.Age = 27;
            bundle.Income = 55000;
            Assert.True(gold.AreConditionsMet(bundle));

            bundle.Age = -1;
            Assert.False(gold.AreConditionsMet(bundle));
        }
    }
}
