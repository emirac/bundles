﻿using Bundles.Entities;
using Bundles.Entities.Conditions;
using Bundles.Entities.Helpers;
using Xunit;

namespace Tests
{
    public class ConditionsTest
    {
        [Fact]
        public void HasAccountConditionTest()
        {
            var condition = new HasAccountCondition();

            var bundle1 = new BundleDto();
            var product1 = new ProductDto();
            product1.Name = ProductsHelper.CurrentAccount;
            bundle1.Products.Add(product1);

            var bundle2 = new BundleDto();
            var product2 = new ProductDto();
            product2.Name = ProductsHelper.CreditCard;
            bundle2.Products.Add(product2);

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void HighIncomeConditionTest()
        {
            var condition = new HighIncomeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Income = 50000;

            var bundle2 = new BundleDto();
            bundle2.Income = 500;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void IsStudentConditionTest()
        {
            var condition = new IsStudentCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Student = true;

            var bundle2 = new BundleDto();
            bundle2.Student = false;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void MediumAgeConditionTest()
        {
            var condition = new MediumAgeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Age = 35;

       
            var bundle2 = new BundleDto();
            bundle2.Age = -1;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void MediumIncomeConditionTest()
        {
            var condition = new MediumIncomeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Income = 15000;
        
            var bundle2 = new BundleDto();
            bundle2.Income = 150;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void MinimalAgeCondition()
        {
            var condition = new MinimalAgeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Age = 15;

            var bundle2 = new BundleDto();
            bundle2.Age = 21;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void MinimalIncomeConditionTest()
        {
            var condition = new MinimalIncomeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Income = 15;

            var bundle2 = new BundleDto();
            bundle2.Income = -1;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void NonMediumAgeConditionTest()
        {
            var condition = new NonMediumAgeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Age = 17;

            var bundle2 = new BundleDto();
            bundle2.Age = 21;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }

        [Fact]
        public void NonMinimalAgeConditionTest()
        {
            var condition = new NonMinimalAgeCondition();
            
            var bundle1 = new BundleDto();
            bundle1.Age = 25;

            var bundle2 = new BundleDto();
            bundle2.Age = 14;

            Assert.True(condition.IsConditionMet(bundle1));
            Assert.False(condition.IsConditionMet(bundle2));
        }
    }
}
