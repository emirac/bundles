﻿using Bundles.Entities;
using Bundles.Entities.DefaultProducts;
using Bundles.Entities.Helpers;
using Xunit;

namespace Tests
{
    public class DefaultProductsTest
    {
        [Fact]
        public void CurrentAccountTest()
        {
            var account = new CurrentAccount();

            var bundle1 = new BundleDto();
            bundle1.Age = 25;
            bundle1.Income = 20;
        
            var bundle2 = new BundleDto();
            bundle2.Age = 5;

            var bundle3 = new BundleDto();
            bundle3.Income = -1;

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
            Assert.False(account.AreConditionsMet(bundle3));
        }

        [Fact]
        public void JuniorSaverAccountTest()
        {
            var account = new JuniorSaverAccount();

            var bundle1 = new BundleDto();
            bundle1.Age = 16;

            var bundle2 = new BundleDto();
            bundle2.Age = 26;

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
        }

        [Fact]
        public void StudentAccountTest()
        {
            var account = new StudentAccount();

            var bundle1 = new BundleDto();
            bundle1.Age = 16;
            bundle1.Student = true;

            var bundle2 = new BundleDto();
            bundle2.Age = 26;

            var bundle3 = new BundleDto();
            bundle3.Student = false;

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
            Assert.False(account.AreConditionsMet(bundle3));
        }

        [Fact]
        public void CurrentAccountPlusTest()
        {
            var account = new CurrentAccountPlus();

            var bundle1 = new BundleDto();
            bundle1.Age = 25;
            bundle1.Income = 45000;

            var bundle2 = new BundleDto();
            bundle2.Age = 15;

            var bundle3 = new BundleDto();
            bundle3.Income = 450;

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
            Assert.False(account.AreConditionsMet(bundle3));
        }

        [Fact]
        public void DebitCardTest()
        {
            var account = new DebitCard();

            var bundle1 = new BundleDto();
            var product1 = new ProductDto();
            product1.Name = ProductsHelper.CurrentAccount;
            bundle1.Products.Add(product1);

            var bundle2 = new BundleDto();
            var product2 = new ProductDto();
            product2.Name = ProductsHelper.CreditCard;
            bundle2.Products.Add(product2);

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
        }

        [Fact]
        public void CreditCardTest()
        {
            var account = new CreditCard();

            var bundle1 = new BundleDto();
            bundle1.Age = 19;
            bundle1.Income = 15000;

            var bundle2 = new BundleDto();
            bundle2.Age = 16;

            var bundle3 = new BundleDto();
            bundle3.Income = 150;

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
            Assert.False(account.AreConditionsMet(bundle3));
        }

        [Fact]
        public void GoldCreditCardTest()
        {
            var account = new GoldCreditCard();

            var bundle1 = new BundleDto();
            bundle1.Age = 26;
            bundle1.Income = 45000;

            var bundle2 = new BundleDto();
            bundle2.Age = 66;

            var bundle3 = new BundleDto();
            bundle3.Income = 39000;

            Assert.True(account.AreConditionsMet(bundle1));
            Assert.False(account.AreConditionsMet(bundle2));
            Assert.False(account.AreConditionsMet(bundle3));
        }
    }
}
